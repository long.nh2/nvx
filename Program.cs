﻿// using Amazon.IoT;
// using Amazon.IoT.Model;
// using System;
// using System.Collections.Generic;
// using System.Globalization;
// using System.IO;
// using System.Net;
// using System.Net.Http;
// using System.Reflection.Metadata;
// using System.Security.Cryptography;
// using System.Text;
// using System.Threading.Tasks;


// class Register
// {
//     public async Task CreateThingsInBulk(string deviceName)
//     {

//         var iotClient = new AmazonIoTClient(accessKeyId, secretAccessKey, "us-west1");

//         var request = new StartThingRegistrationTaskRequest
//         {
//             InputFileBucket = "ebeebucket",
//             InputFileKey = "things.json",
//             RoleArn = "arn:aws:iam::396141340911:role/eBeeS3Role",
//             TemplateBody = @"{
//             ""Parameters"" : {
//                 ""ThingName"" : {
//                     ""Type"" : ""String""
//                 },
//                 ""SerialNumber"" : {
//                     ""Type"" : ""String""
//                 },
//                 ""Location"" : {
//                     ""Type"" : ""String"",
//                     ""Default"" : ""WA""
//                 },
//                 ""CSR"" : {
//                     ""Type"" : ""String""    
//                 }
//             }
//         }"
//         };

//         try
//         {
//             var response = await iotClient.StartThingRegistrationTaskAsync(request);
//             var list = await iotClient.ListThingsAsync();
//             Console.WriteLine($"Bulk thing provisioning task ID: {response.TaskId}");
//         }
//         catch (AmazonIoTException e)
//         {
//             Console.WriteLine($"Error: {e.Message}");
//         }

//     }


//     public static void Main(string[] args)
//     {
//         // Giả sử bạn đã lưu trữ thông tin đăng nhập AWS một cách an toàn
//         string accessKeyId = "AKIAVYO65FDXQBTYAHAO";
//         string secretAccessKey = "5bEpYBECVNzNeehCs5xJ/KUd+6hf0kMirrvbY8US";

//         // Thay thế bằng tên thiết bị thực tế bạn muốn sử dụng
//         string deviceName = "YOUR_DEVICE_NAME";
//         try
//         {
//             // Tạo một thể hiện của lớp (giả sử tên là 'IoTManager' hoặc tương tự)
//             var iotManager = new AmazonIoTClient(accessKeyId, secretAccessKey);

//             // Gọi phương thức CreateThingsInBulk trên thể hiện
//             Task task = AmazonIoTClient.CreateThingsInBulk(deviceName);

//             // ... (Phần còn lại của mã vẫn giống như trước)
//         }
//         catch (Exception ex)
//         {
//             // ... (Xử lý lỗi vẫn giống như trước)
//         }

//         try
//         {
//             // Gọi hàm CreateThingsInBulk một cách bất đồng bộ
//             Task task = CreateThingsInBulk(deviceName);

//             // Chờ tác vụ hoàn thành
//             await task;

//             Console.WriteLine("Cung cấp thiết bị số lượng lớn thành công!");
//         }
//         catch (Exception ex)
//         {
//             Console.WriteLine($"Xảy ra lỗi: {ex.Message}");
//         }
//     }
// }
// // using System;
// // using System.Diagnostics;

// // internal class Program
// // {
// //     private static readonly Stopwatch watch = new Stopwatch();
// //     private static void Main(string[] args)
// //     {
// //         watch.Start();
// //         TaskAsyncTest();
// //         // Console.Read();
// //     }
// //     /*task async*/
// //     private static void TaskAsyncTest()
// //     {
// //         Task task1 = Task1Async();
// //         Task task2 = Task2Async();
// //         Task.WaitAll(task1, task2);
// //         Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds} ms");
// //     }
// //     private static async Task Task1Async()
// //     {
// //         await Task.Delay(5000);
// //         Console.WriteLine($"TaskOne\tExecution Time: {watch.ElapsedMilliseconds} ms");
// //     }
// //     private static async Task Task2Async()
// //     {
// //         await Task.Delay(2000);
// //         Console.WriteLine($"TaskTwo\tExecution Time: {watch.ElapsedMilliseconds} ms");
// //     }
// // }
// // using System;
// // using System.Threading.Tasks;

// // namespace AsyncBreakfast
// // {
// //     // These classes are intentionally empty for the purpose of this example. They are simply marker classes for the purpose of demonstration, contain no properties, and serve no other purpose.
// //     internal class Bacon { }
// //     internal class Coffee { }
// //     internal class Egg { }
// //     internal class Juice { }
// //     internal class Toast { }

// //     class Program
// //     {
// //         static void Main(string[] args)
// //         {
// //             Coffee cup = PourCoffee();
// //             Console.WriteLine("coffee is ready");

// //             Egg eggs = FryEggs(2);
// //             Console.WriteLine("eggs are ready");

// //             Bacon bacon = FryBacon(3);
// //             Console.WriteLine("bacon is ready");

// //             Toast toast = ToastBread(2);
// //             ApplyButter(toast);
// //             ApplyJam(toast);
// //             Console.WriteLine("toast is ready");

// //             Juice oj = PourOJ();
// //             Console.WriteLine("oj is ready");
// //             Console.WriteLine("Breakfast is ready!");
// //         }

// //         private static Juice PourOJ()
// //         {
// //             Console.WriteLine("Pouring orange juice");
// //             return new Juice();
// //         }

// //         private static void ApplyJam(Toast toast) =>
// //             Console.WriteLine("Putting jam on the toast");

// //         private static void ApplyButter(Toast toast) =>
// //             Console.WriteLine("Putting butter on the toast");

// //         private static Toast ToastBread(int slices)
// //         {
// //             for (int slice = 0; slice < slices; slice++)
// //             {
// //                 Console.WriteLine("Putting a slice of bread in the toaster");
// //             }
// //             Console.WriteLine("Start toasting...");
// //             Task.Delay(3000).Wait();
// //             Console.WriteLine("Remove toast from toaster");

// //             return new Toast();
// //         }

// //         private static Bacon FryBacon(int slices)
// //         {
// //             Console.WriteLine($"putting {slices} slices of bacon in the pan");
// //             Console.WriteLine("cooking first side of bacon...");
// //             Task.Delay(3000).Wait();
// //             for (int slice = 0; slice < slices; slice++)
// //             {
// //                 Console.WriteLine("flipping a slice of bacon");
// //             }
// //             Console.WriteLine("cooking the second side of bacon...");
// //             Task.Delay(3000).Wait();
// //             Console.WriteLine("Put bacon on plate");

// //             return new Bacon();
// //         }

// //         private static Egg FryEggs(int howMany)
// //         {
// //             Console.WriteLine("Warming the egg pan...");
// //             Task.Delay(3000).Wait();
// //             Console.WriteLine($"cracking {howMany} eggs");
// //             Console.WriteLine("cooking the eggs ...");
// //             Task.Delay(3000).Wait();
// //             Console.WriteLine("Put eggs on plate");

// //             return new Egg();
// //         }

// //         private static Coffee PourCoffee()
// //         {
// //             Console.WriteLine("Pouring coffee");
// //             return new Coffee();
// //         }
// //     }
// // }
// using System;
// using System.Threading.Tasks;
// using Amazon.IoT;
// using Amazon.IoT.Model;
// using static Amazon.RegionEndpoint;

// public class AWSThigsProvisioning
// {
//     private static readonly string AccessKey = "AKIAVYO65FDXQBTYAHAO"; // Replace with your access key
//     private static readonly string SecretKey = "5bEpYBECVNzNeehCs5xJ/KUd+6hf0kMirrvbY8US"; // Replace with your secret key
//     private static readonly string Region = "us-east1"; // Replace with your region

//     public static async Task Main(string[] args)
//     {
//         var deviceName = "YourDeviceName"; // Thay thế bằng tên thiết bị của bạn

//         try
//         {
//             var iotClient = new AmazonIoTClient(AccessKey, SecretKey, Region);

//             await CreateThingsInBulk(iotClient, deviceName);
//         }
//         catch (Exception e)
//         {
//             Console.WriteLine($"Error: {e.Message}");
//         }
//     }

//     public static async Task CreateThingsInBulk(AmazonIoTClient iotClient, string deviceName)
//     {
//         var request = new StartThingRegistrationTaskRequest
//         {
//             InputFileBucket = "ebeebucket",
//             InputFileKey = "things.json",
//             RoleArn = "arn:aws:iam::396141340911:role/eBeeS3Role",
//             TemplateBody = @"{
//                 ""Parameters"" : {
//                     ""ThingName"" : {
//                         ""Type"" : ""String""
//                     },
//                     ""SerialNumber"" : {
//                         ""Type"" : ""String""
//                     },
//                     ""Location"" : {
//                         ""Type"" : ""String"",
//                         ""Default"" : ""WA""
//                     },
//                     ""CSR"" : {
//                         ""Type"" : ""String""    
//                     }
//                 }
//             }"
//         };

//         try
//         {
//             var response = await iotClient.StartThingRegistrationTaskAsync(request);
//             var list = await iotClient.ListThingsAsync();
//             Console.WriteLine($"Bulk thing provisioning task ID: {response.TaskId}");
//         }
//         catch (AmazonIoTException e)
//         {
//             Console.WriteLine($"Error: {e.Message}");
//         }
//     }
// }

using System;
using System.Threading.Tasks;

// NuGet packages: AWSSDK.S3, AWSSDK.SecurityToken, AWSSDK.SSO, AWSSDK.SSOOIDC
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.IoT;
using Amazon.IoT.Model;
using System.Text.Json;

namespace S3CreateAndList
{
    class Program
    {
        static void Main(string[] args)
        {
            var program = new Program();
            string deviceName = "device"; // Replace with your desired device name
            program.CreateThingsInBulk(deviceName).Wait();
        }

        public async Task CreateThingsInBulk(string deviceName)
        {
            var ssoCreds = LoadSsoCredentials("default");

            // Display the caller's identity.
            var ssoProfileClient = new AmazonSecurityTokenServiceClient(ssoCreds);
            Console.WriteLine($"\nSSO Profile:\n {await ssoProfileClient.GetCallerIdentityArn()}");

            // Create the S3 client using the SSO credentials obtained earlier.
            var iotClient = new AmazonIoTClient(ssoCreds);
            string temPlate = File.ReadAllText(@"./template.json");


            var request = new StartThingRegistrationTaskRequest
            {
                InputFileBucket = "ebeebucket",
                InputFileKey = "things.json", // Contains newline-delimited JSON for provisioning devices
                RoleArn = "arn:aws:iam::396141340911:role/eBeeS3Role",
                TemplateBody = temPlate
            };

            try
            {
                var response = await iotClient.StartThingRegistrationTaskAsync(request);
                var list = await iotClient.ListThingsAsync();
                // Console.WriteLine(list);
                // Console.WriteLine(response);
                Console.WriteLine($"Bulk thing provisioning task ID: {response.TaskId}");
            }
            catch (AmazonIoTException e)
            {
                Console.WriteLine($"Error: {e.Message}");
                // Consider throwing an exception or implementing retries here
            }
        }

        static AWSCredentials LoadSsoCredentials(string profile)
        {
            var chain = new CredentialProfileStoreChain();
            if (!chain.TryGetAWSCredentials(profile, out var credentials))
            {
                throw new Exception($"Failed to find the {profile} profile");
            }
            return credentials;
        }
    }
    // Class to read the caller's identity.
    public static class Extensions
    {
        public static async Task<string> GetCallerIdentityArn(this IAmazonSecurityTokenService stsClient)
        {
            var response = await stsClient.GetCallerIdentityAsync(new GetCallerIdentityRequest());
            return response.Arn;
        }
    }

}
